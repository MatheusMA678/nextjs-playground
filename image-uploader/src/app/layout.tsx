import './globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import { ThemeProvider } from '@/components/theme-provider'
import { Header } from '@/components/header'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Image Uploader',
  description: 'Não perca suas fotos favoritas, faça upload delas no Image Uploader.',
}

export default function RootLayout(props: {
  children: React.ReactNode,
  image: React.ReactNode,
}) {
  return (
    <html lang="pt-BR" suppressHydrationWarning>
      <body className={inter.className}>
        <ThemeProvider
          attribute="class"
          defaultTheme="system"
          enableSystem
          disableTransitionOnChange
        >
          <Header />
          {props.children}
          {props.image}
        </ThemeProvider>
      </body>
    </html>
  )
}
