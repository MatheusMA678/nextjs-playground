import { Modal } from "@/components/modal";
import { Button } from "@/components/ui/button";
import { prisma } from "@/services/prisma";
import Image from 'next/image'
import { Cross1Icon } from '@radix-ui/react-icons'

export default async function ImageInfo({ params: { id } }: {
  params: {
    id: string
  }
}) {
  const image = await prisma.image.findUnique({
    where: {
      id,
    }
  })

  return (
    <Modal>
      {!image ? (
        <div>
          <strong>Imagem não encontrada.</strong>
        </div>
      ) : (
        <Image
          src={image.url}
          alt={image.name}
          width={500}
          height={500}
          className="max-h-full object-cover rounded-lg"
        />
      )}
    </Modal>
  )
}
