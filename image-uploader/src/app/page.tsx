import { getServerSession } from "next-auth"

import { authOptions } from "@/lib/auth"
import { uploadImage } from "@/server/actions"
import { prisma } from "@/services/prisma"

import { ImageInput } from "@/components/image-input"
import { Input } from "@/components/ui/input"
import { Label } from "@/components/ui/label"
import { SubmitButton } from "@/components/submit-button"
import { ImageContent } from "@/components/image"
import { z } from 'zod'
import { ImageForm } from "@/components/image-form"

export default async function Home() {
  const session = await getServerSession(authOptions)
  const images = await prisma.image.findMany({
    where: {
      userId: session?.user.id
    }
  })

  return (
    <div className="container flex flex-col md:flex-row gap-8">
      <ImageForm isLogged={!!session} />

      <main className="relative flex flex-col gap-6 py-8 md:pr-2 md:overflow-y-auto md:max-h-[calc(100vh-80px)] w-full">
        {
          !!session
            ? images.length !== 0
              ? (
                <section data-logged={!!session} className="data-[logged=true]:columns-2 gap-4 data-[logged=false]:h-full">
                  {images.map(image => <ImageContent key={image.id} image={image} />)}
                </section>
              )
              : (
                <section className="w-full h-full flex items-center justify-center">
                  <strong>Não há imagens... Faça o upload para vê-las aqui!!</strong>
                </section>
              )
            : (
              <section className="w-full h-full flex items-center justify-center">
                <strong>Faça login para ver suas imagens!</strong>
              </section>
            )
        }
      </main>
    </div>
  )
}
