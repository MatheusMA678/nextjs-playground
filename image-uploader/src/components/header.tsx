import { getServerSession } from "next-auth";
import { ToggleThemeDropdown } from "./toggle-theme-dropdown";
import { authOptions } from "@/lib/auth";
import { AuthButton } from "./auth-button";
import { UserButtonDropdown } from "./user-button-dropdown";
import { DropdownMenu, DropdownMenuContent, DropdownMenuTrigger } from "./ui/dropdown-menu";
import { Button } from "./ui/button";
import { UploadIcon } from "@radix-ui/react-icons";

export async function Header() {
  const session = await getServerSession(authOptions)

  return (
    <header className="border-b h-20 flex items-center">
      <div className="container flex justify-between items-center">
        <h1 className="text-2xl md:text-4xl font-bold">Uploader</h1>

        <div className="flex gap-4 items-center">
          <DropdownMenu>
            <DropdownMenuTrigger asChild>
              <Button variant="outline">
                <UploadIcon />
              </Button>
            </DropdownMenuTrigger>
            <DropdownMenuContent>

            </DropdownMenuContent>
          </DropdownMenu>

          <ToggleThemeDropdown />

          {
            session
              ? <UserButtonDropdown userImage={session.user.image} />
              : <AuthButton />
          }
        </div>
      </div>
    </header>
  )
}
