import { uploadImage } from '@/server/actions'
import { Label } from '@/components/ui/label'
import { Input } from '@/components/ui/input'
import { ImageInput } from './image-input'
import { SubmitButton } from './submit-button'

export function ImageForm({ isLogged }: { isLogged: boolean }) {
  return (
    <form action={uploadImage} className="flex-col gap-6 py-8 w-full max-w-[500px] hidden md:flex">
      <fieldset className="flex flex-col gap-2">
        <Label>Nome</Label>
        <Input required type="text" id="name" name="name" placeholder="Dê um nome a sua imagem" />
      </fieldset>
      <ImageInput />
      <SubmitButton isLogged={isLogged} />
    </form>
  )
}
