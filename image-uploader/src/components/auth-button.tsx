'use client'

import { Button } from "./ui/button";
import { signIn } from 'next-auth/react'

export function AuthButton() {
  return (
    <Button onClick={() => signIn('github')}>Entrar</Button>
  )
}
