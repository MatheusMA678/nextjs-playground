'use client'

import { DropdownMenu, DropdownMenuTrigger, DropdownMenuContent, DropdownMenuItem } from "@/components/ui/dropdown-menu";
import { signOut } from "next-auth/react";
import Image from "next/image";

export function UserButtonDropdown({ userImage }: { userImage: string | null | undefined }) {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger className="rounded-full w-8 h-8 shadow-lg overflow-hidden">
        <Image
          src={userImage!}
          alt=""
          width={128}
          height={128}
          className="aspect-square w-full h-full object-cover"
        />
      </DropdownMenuTrigger>
      <DropdownMenuContent>
        <DropdownMenuItem onClick={() => signOut()} className="text-red-500 hover:text-red-500">Sair</DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  )
}
