'use client'

import { Cross1Icon } from "@radix-ui/react-icons";
import { Button } from "./ui/button";
import { useRouter } from "next/navigation";

export function Modal({ children }: { children: React.ReactNode }) {
  const router = useRouter()

  return (
    <div className="absolute inset-0 w-full h-full flex items-center justify-center bg-background/20 backdrop-blur">
      <Button
        onClick={() => router.back()}
        className="absolute right-8 top-8"
        variant="ghost"
        size="icon"
      >
        <Cross1Icon />
      </Button>
      {children}
    </div>
  )
}
