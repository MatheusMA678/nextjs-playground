import { Image as ImageType } from "@prisma/client";
import Image from "next/image";
import Link from "next/link";

interface ImageContentProps {
  image: ImageType
}

export function ImageContent({ image }: ImageContentProps) {
  function formatBytes(bytes: number, decimals = 2) {
    if (bytes === 0) return '0 Bytes';
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  return (
    <Link
      href={`/photo/${image.id}`}
      className="block rounded-md overflow-hidden relative group break-inside-avoid mb-4">
      <Image
        src={image.url}
        alt={image.name}
        width={500}
        height={500}
        priority
        className="w-full object-cover rounded-lg"
      />

      <div className="absolute inset-0 bg-background/50 p-4 flex flex-col items-start justify-start opacity-0 group-hover:opacity-100 transition">
        <strong className="text-xl">{image.name}</strong>
        <span className="text-sm text-muted-foreground">{formatBytes(image.size)}</span>
      </div>
    </Link>
  )
}
