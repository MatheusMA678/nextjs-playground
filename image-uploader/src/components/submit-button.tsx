'use client'

import { experimental_useFormStatus as useFormStatus } from 'react-dom'
import { Button } from './ui/button'
import { SymbolIcon } from '@radix-ui/react-icons'

export function SubmitButton({ isLogged }: { isLogged: boolean }) {
  const { pending } = useFormStatus()

  return (
    <Button type="submit" disabled={!isLogged || pending} aria-disabled={!isLogged || pending} className="disabled:cursor-not-allowed">
      {pending && <SymbolIcon className="animate-spin" />}
      {!pending && 'Upload'}
    </Button>
  )
}
