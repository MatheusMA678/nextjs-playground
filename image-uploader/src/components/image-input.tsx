'use client'

import React, { useMemo, useState } from "react";
import { Input } from "./ui/input";
import Image from "next/image";

export function ImageInput() {
  const [imageFile, setImageFile] = useState<File | null>(null)

  const handleImage = ({ target: { files } }: React.ChangeEvent<HTMLInputElement>) => {
    if (files) {
      setImageFile(files[0])
    }
  }

  const imageFileURL = useMemo(() => {
    if (!imageFile) return null

    return URL.createObjectURL(imageFile)
  }, [imageFile])

  return (
    <label htmlFor="image" className="flex items-center justify-center aspect-video w-full rounded-lg border border-dashed bg-foreground/5 cursor-pointer hover:bg-foreground/10 transition-colors overflow-hidden">
      {imageFileURL ? (
        <Image
          src={imageFileURL}
          alt=""
          width={500}
          height={500}
          className="w-full h-full object-cover pointer-events-none"
        />
      ) : (
        <>
          <strong className="text-sm md:text-base text-muted-foreground">Escolher imagem</strong>
        </>
      )}
      <Input required type="file" id="image" name="image" onChange={handleImage} className="sr-only" />
    </label>
  )
}
