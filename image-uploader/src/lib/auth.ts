import { NextAuthOptions } from "next-auth"
import { SupabaseAdapter } from "@next-auth/supabase-adapter"
import GithubProvider from 'next-auth/providers/github'

const clientId = process.env.NEXT_PUBLIC_GITHUB_APP_CLIENT_ID as string
const clientSecret = process.env.NEXT_PUBLIC_GITHUB_APP_CLIENT_SECRET as string

export const authOptions: NextAuthOptions = {
  secret: process.env.NEXTAUTH_SECRET,
  providers: [
    GithubProvider({
      clientId,
      clientSecret,
    })
  ],
  adapter: SupabaseAdapter({
    url: process.env.NEXT_PUBLIC_SUPABASE_URL as string,
    secret: process.env.SUPABASE_SERVICE_ROLE_KEY as string,
  }),
  callbacks: {
    session: async ({ session, user }) => {
      if (session?.user) {
        session.user.id = user.id;
      }
      return session;
    },
  }
}