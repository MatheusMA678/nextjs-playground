"use server"

import { authOptions } from "@/lib/auth";
import { prisma } from "@/services/prisma";
import { supabase } from "@/services/supabase";
import { getServerSession } from 'next-auth'
import { revalidatePath } from "next/cache";
import { randomUUID } from "node:crypto";

export async function uploadImage(
  formData: FormData,
) {
  const img = formData.get('image') as File

  const session = await getServerSession(authOptions)

  if (!session) throw new Error(JSON.stringify({
    message: 'Faça login antes de fazer upload.',
    session
  }))

  const { data: image, error } = await supabase.storage.from('images').upload(
    `${img.name}`,
    img,
    {
      contentType: img.type,
      upsert: true
    }
  )

  if (error) {
    throw new Error(JSON.stringify(error))
  }

  const { data } = supabase.storage.from('images').getPublicUrl(image.path)

  await prisma.image.create({
    data: {
      name: formData.get('name') as string,
      url: data.publicUrl,
      size: img.size,
      userId: session.user.id
    }
  })

  revalidatePath('/')
}