/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    serverActions: true
  },
  images: {
    domains: [
      "bletametauxvgcliqrxc.supabase.co",
      "avatars.githubusercontent.com"
    ]
  },
  env: {
    NEXTAUTH_SECRET: "6J53pvLKp+5vkCP2D+JyTbqTQQHcvDrkr9gNN6MuJpM=",
    NEXTAUTH_URL: "https://3000-matheusma67-nextjsplayg-zwdjt0b0jps.ws-us105.gitpod.io/"
  }
}

module.exports = nextConfig
