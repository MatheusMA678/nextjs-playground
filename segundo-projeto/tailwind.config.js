/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    keyframes: {
      fadeSlideTop: {
        "0%": {
          transform: "translateY(2rem)",
        },
        "50%": {
          transform: "translateY(0)",
        },
        "100%": {
          opacity: 1,
        },
      },
    },
    extend: {
      backgroundColor: {
        app: "#272727",
      },
      animation: {
        fadeSlideTop: "fadeSlideTop 2s forwards",
      },
    },
  },
  plugins: [],
};
