import { AppProps } from "@/@types/types";
import { NextFont } from "next/dist/compiled/@next/font";
import React from "react";

const navOptions = [
  { id: 1, title: "Home" },
  { id: 2, title: "Mentorship" },
  { id: 3, title: "Portfolio" },
  { id: 4, title: "Snippet" },
  { id: 5, title: "Blog" },
];

export function Header({ poppins }: AppProps) {
  return (
    <header className="py-[45px] border-b border-gray-500 flex justify-between items-center">
      <h1 className={`${poppins} font-bold text-2xl`}>Matheus</h1>
      <nav>
        <ul className="flex gap-8">
          {navOptions.map((opt) => (
            <li key={opt.id}>
              <a
                href=""
                className="block font-semibold text-[#D1D1D1] transition hover:text-white"
              >
                {opt.title}
              </a>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  );
}
