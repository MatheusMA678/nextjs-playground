import React from "react";
import { AppProps } from "@/@types/types";
import {
  ArrowRight,
  FacebookLogo,
  InstagramLogo,
  TwitterLogo,
} from "@phosphor-icons/react";
import { CardCarousel } from "./MainComponents";

export function Main({ poppins }: AppProps) {
  return (
    <main className="mt-24 flex flex-col gap-16">
      <section>
        <h1
          className={`${poppins} text-transparent bg-clip-text bg-gradient-to-br from-green-400 to-blue-400 text-5xl leading-[4rem] font-bold opacity-0 animate-fadeSlideTop`}
        >
          Hi Im Matheus, a special human with some ability to love learning and
          working on teamwork.
        </h1>
      </section>
      <section className="flex gap-8">
        <div className="w-[155px] h-[155px] rounded-full bg-gradient-to-br from-green-400 to-blue-400 p-1">
          <div className="w-full h-full rounded-full bg-[#3A3636]"></div>
        </div>
        <div className="flex flex-col gap-4 w-[500px]">
          <h2 className={`${poppins} font-bold text-3xl`}>Biography</h2>
          <p className="text-lg text-gray-300 bg-clip-text bg-gradient-to-br from-green-400 to-blue-400">
            Getting Buff +1 for learning, Buff +2 for documentation and more
            buff on managing team. Exicited on{" "}
            <span className="text-transparent font-bold">React</span>,{" "}
            <span className="text-transparent font-bold">Research</span> and{" "}
            <span className="text-transparent font-bold">Agile</span>.
          </p>
        </div>
        <div className="ml-auto flex flex-col gap-4">
          <h2 className={`${poppins} font-bold text-3xl`}>Lets connect</h2>
          <div className="flex gap-4">
            <a
              target={"_blank"}
              href="https://instagram.com/matheus_mattos_avelino"
            >
              <InstagramLogo
                size={32}
                weight={"bold"}
                className="transition duration-200 hover:text-green-400"
              />
            </a>
            <a target={"_blank"} href="https://twitter.com/MatheusMA678">
              <TwitterLogo
                size={32}
                weight={"bold"}
                className="transition duration-200 hover:text-green-400"
              />
            </a>
            <a target={"_blank"} href="">
              <FacebookLogo
                size={32}
                weight={"bold"}
                className="transition duration-200 hover:text-green-400"
              />
            </a>
          </div>
        </div>
      </section>
      <section className="flex justify-between items-center gap-12">
        <div className="flex flex-col gap-4">
          <h2 className={`${poppins} text-3xl font-bold`}>What I do</h2>
          <p className="text-gray-300 text-lg bg-clip-text bg-gradient-to-br from-green-400 to-blue-400">
            Build and maintain websites,{" "}
            <span className="text-transparent font-bold">frontend</span> dev
            also have a mentorship called{" "}
            <span className="text-transparent font-bold">MOFON</span>. My motto
            is Beauty and function in equal measure as priority.
          </p>
        </div>
        <CardCarousel />
        {/* <button className="transition active:text-gray-400">
          <ArrowRight size={64} weight={"bold"} />
        </button> */}
      </section>
    </main>
  );
}
