import React from "react";
// import { Carousel } from "react-responsive-carousel";
// import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/react-splide/css/sea-green";
import { Card } from "./Card";
import { Globe, Clipboard } from "@phosphor-icons/react";

const cardInfos = [
  {
    id: 1,
    title: "Web Development",
    text: "You will receive a customized plan for your fitness journey, and lots of support.",
    img: <Globe size={32} weight={"bold"} />,
  },
  {
    id: 2,
    title: "UX Research",
    text: "You will receive a customized plan for your fitness journey, and lots of support.",
    img: <Clipboard size={32} weight={"bold"} />,
  },
  {
    id: 3,
    title: "Card 2",
    text: "You will receive a customized plan for your fitness journey, and lots of support.",
    img: <Globe size={32} weight={"bold"} />,
  },
  {
    id: 4,
    title: "Card 3",
    text: "You will receive a customized plan for your fitness journey, and lots of support.",
    img: <Globe size={32} weight={"bold"} />,
  },
];

export function CardCarousel() {
  return (
    <Splide
      options={{
        type: "slide",
        rewind: true,
        autoplay: true,
        width: "650px",
        drag: true,
        pauseOnHover: false,
        arrows: false,
        gap: "1rem",
        perPage: 2,
        perMove: 1,
      }}
    >
      {cardInfos.map((card) => (
        <SplideSlide className="pt-10">
          <Card
            key={card.id}
            title={card.title}
            text={card.text}
            img={card.img}
          />
        </SplideSlide>
      ))}
    </Splide>
  );
}

{
  /* <Carousel
      showStatus={false}
      width={"300px"}
      showIndicators={false}
      swipeable={true}
      showArrows={false}
      autoPlay={true}
      infiniteLoop={true}
      emulateTouch={true}
    >
      {cardInfos.map((card) => (
        <div className="px-4 pt-10">
          <Card
            key={card.id}
            title={card.title}
            text={card.text}
            img={card.img}
          />
        </div>
      ))}
    </Carousel> */
}
