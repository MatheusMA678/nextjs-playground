import React from "react";
import Image from "next/image";
import { Sora } from "next/font/google";

const sora = Sora({
  subsets: ["latin"],
  weight: ["600"],
});

interface CardProps {
  title: string | any;
  text: string | any;
  img: React.ReactNode;
}

export function Card({ title, text, img }: CardProps) {
  return (
    <div className="bg-gray-700 rounded-xl relative min-w-[260px] h-[190px] p-4 pt-14 text-center flex flex-col gap-4">
      <div className="rounded-full w-[70px] h-[70px] bg-gradient-to-br from-green-400 to-blue-400 absolute -top-[35px] left-1/2 -translate-x-1/2 p-px">
        <div className="w-full h-full flex justify-center items-center rounded-full bg-gray-700 overflow-hidden">
          {img}
        </div>
      </div>
      <h3 className={`${sora.className} font-semibold`}>{title}</h3>
      <p className="text-sm text-[#EFEDE8A6]">{text}</p>
    </div>
  );
}
