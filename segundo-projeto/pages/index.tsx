import Head from "next/head";
import { Poppins, Inter } from "next/font/google";
import { Header, Main } from "@/components";

const poppins = Poppins({
  subsets: ["latin"],
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});

const inter = Inter({
  subsets: ["latin"],
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});

export default function Home() {
  return (
    <>
      <Head>
        <title>Portfolio</title>
        <meta name="description" content="My portfolio maded with NextJS." />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main
        className={`${inter.className} dark:bg-app dark:text-white min-h-screen py-4 px-[150px]`}
      >
        <Header poppins={poppins.className} />
        <Main poppins={poppins.className} />
      </main>
    </>
  );
}
