import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: 'var(--font-roboto)'
      },
      backgroundImage: {
        app: "linear-gradient(to right, #996DFF, #C7AFFF)",
        radial: "radial-gradient(78.75% 95.3% at 31.82% 22.67%, transparent 0%, #09090A 100%)"
      },
      colors: {
        product: {
          purple: {
            DEFAULT: '#996DFF',
            light: '#C7AFFF'
          },
        },
        base: {
          black: '#09090A',
          white: '#FFFFFF',
          gray: {
            DEFAULT: '#C4C4CC',
            dark: '#202024'
          },
        }
      }
    },
  },
  plugins: [],
}
export default config
