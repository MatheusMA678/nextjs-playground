import React from "react";

export function Lightning() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="48"
      height="48"
      fill="none"
      viewBox="0 0 48 48"
    >
      <rect
        width="47"
        height="47"
        x="0.5"
        y="0.5"
        fill="#121214"
        rx="3.5"
      ></rect>
      <rect
        width="47"
        height="47"
        x="0.5"
        y="0.5"
        fill="url(#paint0_linear_833_104)"
        rx="3.5"
      ></rect>
      <path
        fill="#FFAC33"
        d="M33.959 22.434a.668.668 0 00-.626-.434h-8.05l4.644-9.028a.666.666 0 00-1.03-.81L24 16.392l-9.769 8.437A.666.666 0 0014.667 26h8.05l-4.644 9.028a.666.666 0 001.029.81L24 31.608l9.769-8.437a.667.667 0 00.19-.737z"
      ></path>
      <rect
        width="47"
        height="47"
        x="0.5"
        y="0.5"
        stroke="url(#paint1_linear_833_104)"
        rx="3.5"
      ></rect>
      <defs>
        <linearGradient
          id="paint0_linear_833_104"
          x1="24"
          x2="24"
          y1="0"
          y2="48"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#202024"></stop>
          <stop offset="1" stopColor="#202024" stopOpacity="0.2"></stop>
        </linearGradient>
        <linearGradient
          id="paint1_linear_833_104"
          x1="24"
          x2="24"
          y1="0"
          y2="48"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#323238"></stop>
          <stop offset="1" stopColor="#323238" stopOpacity="0.3"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}
