import './globals.css'
import type { Metadata } from 'next'
import { Roboto_Flex } from 'next/font/google'

const roboto = Roboto_Flex({ subsets: ['latin'], variable: '--font-roboto' })

export const metadata: Metadata = {
  title: 'Newsletter - Rocketseat',
  description: 'Inscreva-se para a nossa newsletter para receber as últimas notícias e ficar atualizado!',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="pt-BR">
      <body className={`${roboto.variable} font-sans min-h-screen pt-[150px] pb-20 overflow-x-hidden`}>{children}</body>
    </html>
  )
}
