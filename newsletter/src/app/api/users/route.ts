import { prisma } from "@/lib/prisma"

export async function POST(req: Request) {
  const { email } = await req.json()

  try {
    await prisma.user.create({
      data: {
        email,
      },
    })
    return Response.json({ message: 'Enviado com sucesso.' }, {
      status: 201
    })
  } catch (error) {
    return Response.json({ error }, {
      status: 400,
    })
  }
}