import { Lightning } from "@/assets/lightning"
import { Present } from "@/assets/present"
import { Rocket } from "@/assets/rocket"
import { Tool } from "@/assets/tool"
import { AnimatedDiv } from "@/components/animated-div"
import { AnimatedSection } from "@/components/animated-section"
import { Button } from "@/components/button"
import { ImagesBackground } from "@/components/images-bg"
import { Input } from "@/components/input"
import { Toast } from "@/components/toast"
import { prisma } from "@/lib/prisma"
import { sendUserEmail } from "@/server/actions"
import { Icon } from "@prisma/client"

export default async function Home() {
  const items = await prisma.item.findMany()

  const switchIcons = (icon: Icon) => {
    switch (icon) {
      case 'Rocket':
        return <Rocket />

      case 'Tool':
        return <Tool />

      case 'Lightning':
        return <Lightning />

      case 'Present':
        return <Present />

      default:
        break;
    }
  }

  const container = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        staggerChildren: 0.4
      }
    }
  }

  const itemVariant = {
    hidden: { opacity: 0 },
    show: {
      opacity: 1,
      transition: {
        type: 'spring'
      }
    }
  }

  return (
    <div className="max-w-6xl w-full mx-auto flex flex-col gap-[100px] md:gap-[160px] px-8 overflow-x-hidden">
      <ImagesBackground />
      <Toast />

      <section className="flex flex-col gap-8 z-10">
        <div>
          <h1 className="uppercase bg-app bg-clip-text text-transparent font-semibold tracking-widest leading-none">Newsletter exclusiva</h1>
          <h2 className="text-5xl font-bold pt-4 leading-none">Workspace inspiration</h2>
        </div>

        <p className="text-xl text-base-gray w-full max-w-[445px]">
          Assine nossa newsletter e transforme seu espaço de trabalho em um oásis de produtividade!
        </p>

        <form action={sendUserEmail} className="flex flex-col md:flex-row md:items-center gap-2">
          <Input />
          <Button type="submit" />
        </form>
      </section>

      <AnimatedSection
        variants={container}
        initial="hidden"
        animate="show"
        className="flex flex-col md:flex-row items-start gap-8 md:gap-4 justify-between z-10"
      >
        {items.map(item => {
          return (
            <AnimatedDiv
              key={item.id}
              variants={itemVariant}
              className="flex flex-col gap-2 md:max-w-[270px] w-full"
            >
              {switchIcons(item.icon)}
              <strong className="text-lg mt-2">{item.title}</strong>
              <p className="text-sm text-base-gray">{item.content}</p>
            </AnimatedDiv>
          )
        })}
      </AnimatedSection>
    </div>
  )
}
