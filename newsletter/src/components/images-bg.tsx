import Image1 from '@/assets/images/image_1.png'
import Image2 from '@/assets/images/image_2.png'
import Image3 from '@/assets/images/image_3.png'
import Image4 from '@/assets/images/image_4.png'
import Image5 from '@/assets/images/image_5.png'
import Image6 from '@/assets/images/image_6.png'
import Image7 from '@/assets/images/image_7.png'
import Image from 'next/image'
import { motion } from 'framer-motion'
import { AnimatedDiv } from './animated-div'

const images = {
  left: [
    Image1,
    Image2,
    Image3,
  ],
  right: [
    Image4,
    Image5,
    Image6,
    Image7,
  ]
}

export function ImagesBackground() {
  return (
    <div className="absolute -top-36 -right-36 flex items-center gap-4 after:content-[''] after:bg-radial after:w-full after:h-full after:absolute">
      <AnimatedDiv
        initial={{
          opacity: 0,
          y: 200
        }}
        animate={{
          opacity: 1,
          y: 0
        }}
        transition={{
          ease: 'easeOut',
          duration: 1
        }}
        className="flex flex-col gap-4"
      >
        {images.left.map((src, index) => (
          <Image
            key={index}
            src={src}
            alt=''
            className="max-w-[400px] w-full aspect-video rounded-lg object-cover"
          />
        ))}
      </AnimatedDiv>
      <AnimatedDiv
        initial={{
          opacity: 0,
          y: -200
        }}
        animate={{
          opacity: 1,
          y: 0
        }}
        transition={{
          ease: 'easeOut',
          duration: 1
        }}
        className="flex flex-col gap-4"
      >
        {images.right.map((src, index) => (
          <Image
            key={index}
            src={src}
            alt=''
            className="max-w-[400px] w-full aspect-video rounded-lg object-cover"
          />
        ))}
      </AnimatedDiv>
    </div>
  )
}
