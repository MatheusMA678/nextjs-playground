import React from "react";

export function Input({ ...props }: React.InputHTMLAttributes<HTMLInputElement>) {
  return (
    <input
      type="email"
      name="email"
      id="email"
      aria-label="email"
      required
      placeholder="Digite seu e-mail"
      className="bg-base-gray-dark border border-transparent focus:border-product-purple transition-colors px-4 py-3 rounded-md outline-none w-full md:max-w-[290px]"
      {...props}
    />
  )
}
