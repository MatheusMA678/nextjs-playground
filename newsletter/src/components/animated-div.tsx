'use client'

import React from 'react'
import { MotionProps, motion } from 'framer-motion'

interface AnimatedDivProps extends MotionProps {
  children: React.ReactNode
  className?: string
}

export function AnimatedDiv({
  children,
  className,
  ...props
}: AnimatedDivProps) {
  return (
    <motion.div
      {...props}
      className={className}
    >
      {children}
    </motion.div>
  )
}
