'use client'

import { useSearchParams } from "next/navigation"
import { motion } from "framer-motion"
import { MailCheck } from "lucide-react"

export function Toast() {
  const searchParams = useSearchParams()
  const send = searchParams.get('send')

  return (
    <motion.div
      initial={{
        opacity: 0,
        x: 200
      }}
      animate={{
        x: 0,
        opacity: 1
      }}
      transition={{
        type: 'spring',
        duration: 0.2,
        ease: 'linear'
      }}
      data-send={send}
      className="hidden data-[send=true]:flex gap-4 items-center absolute top-4 right-4 z-50 border border-base-gray-dark bg-base-black rounded-md hover:border-product-purple transition py-3 px-6 shadow-xl"
    >
      <MailCheck className="text-product-purple" />
      <span className="font-medium">E-mail cadastrado com sucesso!</span>
    </motion.div>
  )
}
