'use client'

import { CaretRight } from "@/assets/caret-right";
import React from "react";
import { experimental_useFormStatus as useFormStatus } from "react-dom";

export function Button({ ...props }: React.ButtonHTMLAttributes<HTMLButtonElement>) {
  const { pending } = useFormStatus()

  return (
    <button
      className="bg-app uppercase rounded-md p-4 text-base-black font-bold text-sm flex gap-2 items-center justify-center hover:opacity-80 transition disabled:opacity-50 disabled:cursor-not-allowed"
      disabled={pending}
      {...props}
    >
      quero receber
      <CaretRight />
    </button>
  )
}
