'use client'

import React from 'react'
import { MotionProps, motion } from 'framer-motion'

interface AnimatedSectionProps extends MotionProps {
  children: React.ReactNode
  className?: string
}

export function AnimatedSection({
  children,
  className,
  ...props
}: AnimatedSectionProps) {
  return (
    <motion.section
      {...props}
      className={className}
    >
      {children}
    </motion.section>
  )
}
