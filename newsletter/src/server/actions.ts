'use server'

import { prisma } from "@/lib/prisma"
import { redirect } from "next/navigation"
import { z } from "zod"

const schema = z.object({
  email: z.string().email(),
})

export async function sendUserEmail(formData: FormData) {
  const { email } = schema.parse({
    email: formData.get('email')
  })

  const emailExists = await prisma.user.findUnique({
    where: {
      email
    }
  })

  if (emailExists) return

  await prisma.user.create({
    data: {
      email
    }
  })

  return redirect('/?send=true')
}