# Newsletter

Resolução do #boraCodar de número 40!

> **Deploy:** https://rocketnewsletter.vercel.app/

## Tecnologias
- Next.js
- Tailwind CSS
- Framer Motion
- TypeScript
- PostgreSQL
- Prisma ORM
