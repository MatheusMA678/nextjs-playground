/*
  Warnings:

  - The primary key for the `items` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - The `id` column on the `items` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - Added the required column `icon` to the `items` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "Icon" AS ENUM ('rocket', 'tool', 'present', 'lightning');

-- AlterTable
ALTER TABLE "items" DROP CONSTRAINT "items_pkey",
ADD COLUMN     "icon" "Icon" NOT NULL,
DROP COLUMN "id",
ADD COLUMN     "id" SERIAL NOT NULL,
ADD CONSTRAINT "items_pkey" PRIMARY KEY ("id");
